# Application Syscall Trace Analyzer #

### Currently provided features ###

* Sequence alignment
* Sequence distance computation

### Alignment algorithms ###

* Needleman-Wunsch

### Distance algorithms ###

* Hamming
* Jaccard
* Levenshtein
* Levenshtein-Damerau
* QGram
* Jaro
* Jaro-Winkler
* Cosine
* Longest Common Subsequence

This project is a work in progress.
