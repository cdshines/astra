package com.tkroman.astra

import _root_.metric.Levenshtein
import com.tkroman.astra.align.NeedlemanWunsch

import scala.sys.process._
import scala.language.postfixOps
import com.typesafe.config.ConfigFactory

/**
 * @author tk.roman
 */
object Tracerunner {
  import com.github.kxbmap.configs._
  val config = ConfigFactory.load()

  val Password      = config.get[String]("password")
  val TracerBinary  = config.get[String]("tracer.binary")
  val CommandRegex  = config.get[String]("command.syscall.regex").r
  val Cmd1          = config.get[String]("command.first")
  val Cmd2          = config.get[String]("command.second")
  val SymbolMap     = new Symbolizer()

  def run() = {
    val syscalls1 = syscalls(Cmd1)
    val syscalls2 = syscalls(Cmd2)
    println("Unaligned distance: " + Levenshtein(syscalls1, syscalls2))
    val (alL, alR) = NeedlemanWunsch(syscalls1, syscalls2).flatList
    println("Aligned distance: " + Levenshtein(alL, alR))
  }

  def main(args: Array[String]) {
    run()
  }

  def tracerCommand(cmd: String) = {
    s"sudo -S $TracerBinary $cmd"
  }

  def runTraceProcess(cmd: String, sink: ProcessLogger) = {
    (s"echo $Password" #| tracerCommand(cmd)).lineStream_!(sink)
  }

  def syscalls(cmd: String) = {
    val logger = new TracerLogger
    runTraceProcess(cmd, logger)
    logger.everything
      .map {
        case CommandRegex(call) => Some(call)
        case x: Any => None
      }
      .filter(x => x.isDefined)
      .map(x => SymbolMap.symbolize(x.get))
      .result()
  }
}
