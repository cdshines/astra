package com.tkroman.astra

import scala.collection.GenSeq

/**
 * @author tk.roman
 */
case class QGrams(q: Int) {
  require(q > 0)

  def qGramsWithCount(xs: GenSeq[Int]) = {
    xs.toList.sliding(q)
      .filter(slide => slide.length == q)
      .toList
      .groupBy(identity)
      .map { case (x, y) => (x, y.length) }
      .toMap
  }
}
