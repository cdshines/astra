package com.tkroman.astra.metric

import scala.collection.GenSeq

/**
 * @author tk.roman
 */
abstract class Distance(xs: GenSeq[Int], ys: GenSeq[Int]) {
  def distance: Double
}
