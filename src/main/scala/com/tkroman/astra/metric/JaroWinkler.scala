package com.tkroman.astra.metric

import scala.collection.GenSeq
import scala.collection.mutable.ArrayBuffer

/**
 * @author tk.roman
 */
case class JaroWinkler(xs: GenSeq[Int], ys: GenSeq[Int]) extends Distance(xs, ys) {
  private val jaro = Jaro(xs, ys)

  override def distance = {
    val l = commonPrefixUpTo4()
    val dj = jaro.distance

    val boostThreshold = 0.7 // currently unused
    val p = 0.1
    val winklerDist = dj + l * p * (1 - dj)
    winklerDist
  }

  def commonPrefixUpTo4() =
    (xs zip ys) takeWhile(xy => xy._1 == xy._2) size
}
