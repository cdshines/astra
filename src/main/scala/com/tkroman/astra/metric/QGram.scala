package com.tkroman.astra.metric

import com.tkroman.astra.QGrams

import scala.collection.GenSeq

/**
 * @author tk.roman
 */
case class QGram(xs: GenSeq[Int], ys: GenSeq[Int], q: Int) extends Distance(xs, ys) {
  require(q > 0)
  val FixQ = QGrams(q)

  override def distance = {
    val xsq = FixQ.qGramsWithCount(xs)
    val ysq = FixQ.qGramsWithCount(ys)

    (xsq.keys ++ ysq.keys)
      .toList
      .map(qGram => (xsq.getOrElse(qGram, 0) - ysq.getOrElse(qGram, 0)).abs)
      .sum
  }
}
