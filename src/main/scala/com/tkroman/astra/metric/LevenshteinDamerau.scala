package com.tkroman.astra.metric

import scala.collection.GenSeq
import scala.collection.mutable.ListBuffer

/**
 * @author tk.roman
 */
case class LevenshteinDamerau(xs: GenSeq[Int], ys: GenSeq[Int]) extends Distance(xs, ys) {
  def distance = {
    val alphabetLen = (xs union ys).max + 1

    val Inf = xs.length + ys.length
    val da = ListBuffer.fill(alphabetLen)(0)
    val hl = xs.length + 2
    val hw = ys.length + 2
    val h = ListBuffer.fill(hl, hw)(0)
    h(hl - 1)(hw - 1) = Inf

    for (i <- 0 to xs.length) {
      h(i)(hw - 1) = Inf
      h(i)(0) = i
    }

    for (j <- 0 to ys.length) {
      h(hl - 1)(j) = Inf
      h(0)(j) = j
    }

    for (i <- 1 to xs.length) {
      var db = 0
      for (j <- 1 to ys.length) {
        val i1 = da(ys(j - 1))
        val j1 = db
        val cost = if (xs(i - 1) == ys(j - 1)) {
          db   = j
          0
        } else {
          1
        }
        h(i)(j) =
          (h(i - 1)(j - 1) + cost)    // substitution
            .min(h(i)(j - 1) + 1)     // insertion
            .min(h(i - 1)(j) + 1)     // deletion
            .min(h(idx(i1 - 1, hl))(idx(j1 - 1, hw)) + i - i1 - 1 + 1 + j - j1 - 1)
      }
      da(xs(i - 1)) = i
    }
    h(xs.length)(ys.length)
  }

  private def idx(i: Int, l: Int) = if (i >= 0) {
    i
  } else {
    l + i
  }
}
