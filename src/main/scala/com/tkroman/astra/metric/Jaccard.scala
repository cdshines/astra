package com.tkroman.astra.metric

import com.tkroman.astra.QGrams

import scala.collection.GenSeq

/**
 * @author tk.roman
 */
case class Jaccard(xs: GenSeq[Int], ys: GenSeq[Int], q: Int) extends Distance(xs, ys) {
  require(q > 0)

  val FixQ = QGrams(q)

  override def distance = {
    val qxs = FixQ.qGramsWithCount(xs)
    val qys = FixQ.qGramsWithCount(ys)

    val totalQgrams = qxs.keys ++ qys.keys
    val sharedQgramsCount = totalQgrams
      .toList
      .map {
        case qgr if qxs.contains(qgr) && qys.contains(qgr) => 1
        case _ => 0
      }
      .sum.toDouble

    val totalQgramsCount = totalQgrams.size.toDouble
    1 - (sharedQgramsCount / totalQgramsCount)
  }
}
