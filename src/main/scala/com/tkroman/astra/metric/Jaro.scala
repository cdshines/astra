package com.tkroman.astra.metric

import scala.collection.GenSeq
import scala.collection.mutable.ArrayBuffer

/**
 * @author tk.roman
 */
case class Jaro(xs: GenSeq[Int], ys: GenSeq[Int]) extends Distance(xs, ys) {
  lazy val matchWindowLength = xs.length.max(ys.length) / 2 - 1
  lazy val xsLen = xs.length
  lazy val ysLen = ys.length

  lazy val matches = {
    // map from characters to their positions:
    // char -> (charPosInX, charPosInY)
    val xsMatched = ArrayBuffer[Int]()
    val ysMatched = ArrayBuffer[Int]()

    for (i <- 0 until xsLen) {
      val x = xs(i)
      val xPosInYs = positionInSlice(ys, x, i, ysLen)
      if (xPosInYs.isDefined) {
        xsMatched += i
        ysMatched += xPosInYs.get
      }
    }
    (xsMatched zip ysMatched).toSet
  }

  lazy val transpositions = {
    val halfTranspositions = matches count {
      case (x, y) if x != y => matches.contains((y, x))
      case _                => false
    }
    val transpositions = halfTranspositions.toDouble / 2
    transpositions
  }

  override def distance = {
    val m = matches.size.toDouble
    val proximity = (m / xsLen + m / ysLen + (m - transpositions) / m) / 3d
    // val dist = 1 - proximity
    proximity
  }

  def positionInSlice(ns: GenSeq[Int], n: Int, mid: Int, len: Int): Option[Int] = {
    for (i <- 0.max(mid - matchWindowLength) until len.min(mid + matchWindowLength)) {
      if (ns(i) == n) {
        return Some(i)
      }
    }
    None
  }
}
