package com.tkroman.astra.metric

import scala.collection.GenSeq

/**
 * @author tk.roman
 */
case class Hamming(xs: GenSeq[Int], ys: GenSeq[Int]) extends Distance(xs, ys) {
  override def distance = {
    require(xs.length == ys.length)
    xs zip ys map {
      case (x, y) if x == y => 0
      case (x, y) if x != y => 1
    } sum
  }
}
