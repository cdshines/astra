package metric

import com.tkroman.astra.metric.Distance

import scala.collection.GenSeq

/**
 * @author tk.roman
 */
case class Levenshtein(xs: GenSeq[Int], ys: GenSeq[Int]) extends Distance(xs, ys) {
  def minimum(i1: Int, i2: Int, i3: Int) = i1.min(i2).min(i3)

  override def distance: Double = {
    if (xs == ys) {
      return 0
    }
    if (xs.size == 0) {
      return ys.size
    }
    if (ys.size == 0) {
      return xs.size
    }

    val v0 = Array.tabulate(ys.size + 1)(i => if (i < ys.size) i else 0)
    val v1 = Array.fill(ys.size + 1)(0)

    for (i <- 0 until xs.size) {
      v1(0) = i + 1
      for (j <- 0 until ys.size) {
        val cost = if (xs(i) == ys(j)) {
          0
        } else {
          1
        }
        v1(j + 1) = minimum(v1(j) + 1, v0(j + 1) + 1, v0(j) + cost)
      }
      for (j <- 0 until v0.size) {
        v0(j) = v1(j)
      }
    }
    v1(ys.size)
  }
}