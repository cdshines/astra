package com.tkroman.astra.metric.warp

import com.tkroman.astra.metric.Distance

import scala.collection.GenSeq
import scala.collection.mutable.ListBuffer

/**
 * @author tk.roman
 * WIP, TODO
 */
class DynamicTimeWarp(xs: GenSeq[Int], ys: GenSeq[Int]) extends Distance(xs, ys) {
  override def distance = {
    val dtw: ListBuffer[ListBuffer[Double]] =
      ListBuffer.tabulate(xs.length, ys.length) {
        case (0, 0) => 0
        case (i, 0) => Double.PositiveInfinity
        case (0, j) => Double.PositiveInfinity
        case _      => Double.NaN
      }

    for (i <- 0 until xs.length; j <- 0 until ys.length) {
      val cost = xs(i) - ys(j)
      dtw(i)(j) = cost + dtw(i - 1)(j)
        .min(dtw(i)(j - 1))
        .min(dtw(i - 1)(j - 1))
    }

    dtw(xs.length - 1)(ys.length - 1)
  }
}
