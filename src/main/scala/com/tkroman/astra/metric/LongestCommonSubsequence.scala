package com.tkroman.astra.metric

import scala.collection.GenSeq
import scala.collection.mutable.ListBuffer

/**
 * @author tk.roman
 */
case class LongestCommonSubsequence(xs: GenSeq[Int], ys: GenSeq[Int]) extends Distance(xs, ys) {
  override def distance = {
    val xylcs = lcs(xs, ys)
    // distance(‘ABCvDEx‘,’xABCyzDE’) = 5
    xs.diff(xylcs).size + ys.diff(xylcs).size
  }

  private def lcs(xs: GenSeq[Int], ys: GenSeq[Int]) = {
    if (xs.isEmpty || ys.isEmpty) {
      Nil
    } else if (xs == ys) {
      xs
    } else {
      val lengths = Array.ofDim[Int](xs.size + 1, ys.size + 1)
      for (i <- 0 until xs.size; j <- 0 until ys.size)
        if (xs(i) == ys(j)) {
          lengths(i + 1)(j + 1) = lengths(i)(j) + 1
        } else {
          lengths(i + 1)(j + 1) = lengths(i + 1)(j).max(lengths(i)(j + 1))
        }

      val sb = ListBuffer.newBuilder[Int]
      var x = xs.size
      var y = ys.size
      do {
        if (lengths(x)(y) == lengths(x - 1)(y)) {
          x -= 1
        } else if (lengths(x)(y) == lengths(x)(y-1)) {
          y -= 1
        } else {
          assert(xs(x-1) == ys(y-1))
          sb += xs(x-1)
          x -= 1
          y -= 1
        }
      } while (x > 0 && y > 0)
      sb.result().reverse
    }
  }
}
