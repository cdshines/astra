package com.tkroman.astra.metric

import com.tkroman.astra.QGrams

import scala.collection.GenSeq

/**
 * @author tk.roman
 */
case class Cosine(xs: GenSeq[Int], ys: GenSeq[Int], q: Int) extends Distance(xs, ys) {
  override def distance = {
    val xqs = QGrams(q).qGramsWithCount(xs)
    val yqs = QGrams(q).qGramsWithCount(ys)

    1 - cosineSimilarity(xqs, yqs)
  }

  def cosineSimilarity(xs: Map[List[Int], Int], ys: Map[List[Int], Int]) = {
    val common = (xs.keys ++ ys.keys).toList
    val xsQgramCounts = common.map(qGram => xs.getOrElse(qGram, 0))
    val ysQgramCounts = common.map(qGram => ys.getOrElse(qGram, 0))

    val xsYs = *(xsQgramCounts, ysQgramCounts)
    val xsXs = *(xsQgramCounts, xsQgramCounts)
    val ysYs = *(ysQgramCounts, ysQgramCounts)

    xsYs / math.sqrt(xsXs* ysYs)
  }

  def *(xs: GenSeq[Int], ys: GenSeq[Int]) = dotProduct(xs, ys)

  def dotProduct(xs: GenSeq[Int], ys: GenSeq[Int]) = {
    xs zip ys map {
      case (x, y) => x * y
    } sum
  }
}
