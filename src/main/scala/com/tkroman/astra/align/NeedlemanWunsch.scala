package com.tkroman.astra.align

import scala.collection.GenSeq
import scala.collection.mutable.{ListBuffer, ArrayBuffer}

/**
 * @author tk.roman
 */
case class NeedlemanWunsch(xs: GenSeq[Int], ys: GenSeq[Int]) extends Aligner(xs, ys) {
  override def scoreOnEq = 1
  override def scoreOnNeq = -1
  override val scoreMatrix = buildScoreMatrix()

  def buildScoreMatrix(): List[List[Int]] = {
    val matrix = ListBuffer.fill(xs.size + 1, ys.size + 1)(0)
    for (i <- 0 to xs.size) {
      matrix(i)(0) = -i
    }
    for (j <- 0 to ys.size) {
      matrix(0)(j) = -j
    }

    for (i <- 1 to xs.size; j <- 1 to ys.size) {
      matrix(i)(j) = max(
        matrix(i - 1)(j - 1) + score(i, j),
        matrix(i)(j - 1) - 1,
        matrix(i - 1)(j) - 1
      )
    }
    matrix.map(_.result()).result()
  }
}
