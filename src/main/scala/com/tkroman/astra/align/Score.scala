package com.tkroman.astra.align

import scala.collection.GenSeq

/**
 * @author tk.roman
 */
case class Score(eq: Int, neq: Int, xs: GenSeq[Int], ys: GenSeq[Int]) {
  def score(i: Int, j: Int) =
    if (xs(i - 1) == ys(j - 1)) {
      eq
    } else {
      neq
    }
}
