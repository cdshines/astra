package com.tkroman.astra.align

import scala.collection.GenSeq

/**
 * @author tk.roman
 */
abstract class Aligner(xs: GenSeq[Int], ys: GenSeq[Int]) {
  protected def scoreOnEq: Int
  protected def scoreOnNeq: Int
  protected val score = Score(scoreOnEq, scoreOnNeq, xs, ys)
  protected def score(i: Int, j: Int): Int = score.score(i, j)
  protected def max(ns: Int*) = ns.foldLeft(ns.head)(_.max(_))
  protected def max(ns: GenSeq[Int]) = ns.foldLeft(ns.head)(_.max(_))

  protected def buildScoreMatrix(): List[List[Int]]
  protected val scoreMatrix: GenSeq[GenSeq[Int]] = buildScoreMatrix()

  def flatList = {
    val (l, r) = align()
    (l.map(_.getOrElse(-1)), r.map(_.getOrElse(-1)))
  }

  def align() = {
    var alignmentL = List[Option[Int]]()
    var alignmentR = List[Option[Int]]()
    var i = xs.size
    var j = ys.size

    while (i > 0 || j > 0) {
      if (i > 0 && j > 0 && scoreMatrix(i)(j) == scoreMatrix(i - 1)(j - 1) + score(i, j)) {
        alignmentL ::= Some(xs(i - 1))
        alignmentR ::= Some(ys(j - 1))
        j -= 1
        i -= 1
      } else if (i > 0 && scoreMatrix(i)(j) == scoreMatrix(i - 1)(j) - 1) {
        alignmentL ::= Some(xs(i - 1))
        alignmentR ::= None
        i -= 1
      } else if (j > 0 && scoreMatrix(i)(j) == scoreMatrix(i)(j - 1) - 1) {
        alignmentL ::= None
        alignmentR ::= Some(ys(j - 1))
        j -= 1
      }
    }
    (alignmentL, alignmentR)
  }
}
