package com.tkroman.astra

import java.util.concurrent.atomic.AtomicInteger

import scala.collection.mutable

/**
 * @author tk.roman
 */
class Symbolizer {
  lazy val init = new mutable.HashMap[String, Int]()
  val cnt = new AtomicInteger(0)

  def symbolize(s: String) = {
    init.getOrElseUpdate(s, cnt.getAndIncrement)
  }
}