package com.tkroman.astra

import scala.collection.mutable.ListBuffer
import scala.sys.process.ProcessLogger

/**
 * @author tk.roman
 */
class TracerLogger extends ProcessLogger {
  val sb = ListBuffer.newBuilder[String]

  lazy val everything = {
    sb.result()
  }

  override def out(s: => String) { }

  override def buffer[T](f: => T): T = f

  override def err(s: => String): Unit = if (!s.isEmpty) {
    sb += s
  }
}
