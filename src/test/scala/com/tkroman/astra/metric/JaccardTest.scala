package com.tkroman.astra.metric

/**
 * @author tk.roman
 */
class JaccardTest extends DistanceSuite {
  test("metric") {
    assert(0.5 == Jaccard(a"abcde", a"abdcde", 2).distance)
  }

  test("distance between equal strings is 0") {
    assert(0 == Jaccard(a"abcde", a"abcde", 2).distance)
  }
}
