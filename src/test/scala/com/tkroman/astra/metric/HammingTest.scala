package com.tkroman.astra.metric

/**
 * @author tk.roman
 */
class HammingTest extends DistanceSuite {
  test("distance on nonequal strings") {
    assert(3 == Hamming(a"lol", a"olo").distance)
  }

  test("compares only equal strings") {
    intercept[IllegalArgumentException] {
      Hamming(a"", a"non-empty string").distance
    }
  }

  test("distance between equal strings is 0") {
    assert(0 == Hamming(a"abcd", a"abcd").distance)
    assert(0 == Hamming(a"", a"").distance)
  }
}
