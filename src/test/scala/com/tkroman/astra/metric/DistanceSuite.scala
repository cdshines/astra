package com.tkroman.astra.metric

import org.scalatest.FunSuite

import scala.StringContext._
import scala.collection.GenSeq

/**
 * @author tk.roman
 */
trait DistanceSuite extends FunSuite with MetricSuite {
  implicit class StringCodePoints(private val sc: StringContext) {
    def a(args: Any*) =
      sc.standardInterpolator(treatEscapes, args).codePoints().toArray.toList
  }

  protected def s(a: GenSeq[Option[Int]]): String = {
    a.map(x => x.map(_.toChar).getOrElse('-')).mkString
  }

  protected def s(a: (GenSeq[Option[Int]], GenSeq[Option[Int]])): (String, String) = {
    (s(a._1), s(a._2))
  }
}
