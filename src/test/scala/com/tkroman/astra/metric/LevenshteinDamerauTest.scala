package com.tkroman.astra.metric

/**
 * @author tk.roman
 */
class LevenshteinDamerauTest extends DistanceSuite {
  test("test1") {
    assert(LevenshteinDamerau(a"CA",  a"ABC").distance == 2)
    assert(LevenshteinDamerau(a"abc", a"cba").distance == 2)
    assert(LevenshteinDamerau(a"abc", a"def").distance == 3)
    assert(LevenshteinDamerau(a"abc", a"abc").distance == 0)
  }
}
