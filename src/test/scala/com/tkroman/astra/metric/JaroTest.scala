package com.tkroman.astra.metric
import org.scalactic.Tolerance._

/**
  * @author tk.roman
  */
class JaroTest extends DistanceSuite {
  test("Jaro-Winkler disance examples") {
    val martha = Jaro(a"MARTHA", a"MARHTA")
    val dwayne = Jaro(a"DWAYNE", a"DUANE")
    val dixon  = Jaro(a"DIXON",  a"DICKSONX")

    assert(martha.distance === 0.944 +- 0.01)
    assert(dwayne.distance === 0.822 +- 0.01)
    assert(dixon.distance  === 0.767 +- 0.01)
  }
 }
