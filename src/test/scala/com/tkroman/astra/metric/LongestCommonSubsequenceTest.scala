package com.tkroman.astra.metric

/**
 * @author tk.roman
 */
class LongestCommonSubsequenceTest extends DistanceSuite {
  test("example") {
    assertResult(5)(LongestCommonSubsequence(a"abcvdex", a"xabcyzde").distance)
    assertResult(3)(LongestCommonSubsequence(a"axbycz", a"abc").distance)
    assertResult(3)(LongestCommonSubsequence(a"", a"abc").distance)
    assertResult(3)(LongestCommonSubsequence(a"xaybzc", a"abc").distance)
  }

  test("equal lists") {
    assertResult(0)(LongestCommonSubsequence(a"abcdefg", a"abcdefg").distance)
  }

  test("equal empty lists") {
    assertResult(0)(LongestCommonSubsequence(a"", a"").distance)
  }
}
