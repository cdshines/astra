package com.tkroman.astra.metric

import metric.Levenshtein

/**
 * @author tk.roman
 */
class LevenshteinTest extends DistanceSuite {
  test("Equal lists => 0") {
    assert(0 == Levenshtein(List(1, 2), List(1, 2)).distance)
    assert(0 == Levenshtein(List(), List()).distance)
  }

  test("Dist 1 if 1 different number") {
    assert(1 == Levenshtein(List(1, 2), List(3, 2)).distance)
    assert(1 == Levenshtein(List(2, 2), List(2, 1)).distance)
  }

  test("Dist 2 if 2 different numbers") {
    assert(2 == Levenshtein(List(3, 4), List(2, 3)).distance)
  }

  test("More tests") {
    assert(10 == Levenshtein((1 to 10).toList, (20 to 1 by -1).toList).distance)
  }
}
