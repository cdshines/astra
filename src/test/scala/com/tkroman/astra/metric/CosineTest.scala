package com.tkroman.astra.metric
import org.scalactic.Tolerance._

/**
 * @author tk.roman
 */
class CosineTest extends DistanceSuite {
  test("example 1") {
    assert(Cosine(a"abcde", a"abdcde", 2).distance === 0.329 +- 0.01)
  }
}
