package com.tkroman.astra.metric

/**
 * @author tk.roman
 */
class QGramTest extends DistanceSuite {
  test("distance ") {
    assert(3 == QGram(a"abcde", a"abdcde", 2).distance)
  }
}
