package com.tkroman.astra.metric
import org.scalactic.Tolerance._

/**
 * @author tk.roman
 */
class JaroWinklerTest extends DistanceSuite {
  test("Jaro-Winkler disance examples") {
    val martha = JaroWinkler(a"MARTHA", a"MARHTA")
    val dwayne = JaroWinkler(a"DWAYNE", a"DUANE")
    val dixon  = JaroWinkler(a"DIXON",  a"DICKSONX")

    assert(martha.distance === 0.961 +- 0.01)
    assert(dwayne.distance === 0.84  +- 0.01)
    assert(dixon.distance  === 0.813 +- 0.01)
  }
}
