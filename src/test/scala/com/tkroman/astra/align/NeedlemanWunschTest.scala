package com.tkroman.astra.align

import com.tkroman.astra.metric.DistanceSuite

/**
 * @author tk.roman
 */
class NeedlemanWunschTest extends DistanceSuite {
  val l = a"GATTACA"
  val r = a"GCATGCU"

  test("align with the gap") {
    val nw = NeedlemanWunsch(a"abcdxyzefg", a"abcefg")
    assertResult(("abcdxyzefg", "abc----efg"))(s(nw.align()))
  }

  test("like in wiki") {
    val nw = NeedlemanWunsch(l, r)
    assertResult(("G-ATTACA", "GCA-TGCU"))(s(nw.align()))
  }

  test("like in wiki 2") {
    val nw = NeedlemanWunsch(a"GAAAAAAT", a"GAAT")
    assertResult(("GAAAAAAT", "G----AAT"))(s(nw.align()))
  }

  test("this is unexpected, but good") {
    val nw = NeedlemanWunsch(a"AGACTAGTTAC", a"CGAGACGT").align()
    println(s"""!FIXME!
               |AGACTAGTTAC
               |CGA---GACGT
               |${s(nw)._1}
               |${s(nw)._2}""".stripMargin
    )
  }

  test("align unalignable") {
    val nw = NeedlemanWunsch(a"abcda", a"cba")
    assertResult(("abcda", "--cba"))(s(nw.align()))
  }

  test("cats and dogs") {
    val nw = NeedlemanWunsch(a"cats", a"dogs")
    assertResult(("cats", "dogs"))(s(nw.align()))
  }

  test("commutative?") {
    val nw0 = NeedlemanWunsch(r, l)
    val alignment0 = nw0.align()
    
    val nw1 = new NeedlemanWunsch(l, r)
    val alignment1 = nw0.align()

    assert(alignment0 == alignment1)
  }
}
